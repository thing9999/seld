package co.csad.selendroid;

import org.json.JSONArray;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import bolts.Continuation;
import bolts.Task;
import io.selendroid.client.DriverCommand;
import io.selendroid.client.SelendroidDriver;
import io.selendroid.common.SelendroidCapabilities;
import io.selendroid.standalone.SelendroidConfiguration;
import io.selendroid.standalone.SelendroidLauncher;

public class MobileRemoteTest {
    @Rule
    public RepeatRule repeatRule = new RepeatRule();

    /**
     * export ANDROID_HOME=/Users/bigparksh/Library/Android/sdk
     * java -jar selendroid-standalone-0.17.0-with-dependencies.jar -app selendroid-test-app-0.17.0.apk
     */
    private static SelendroidLauncher mSelendroidServer;
    private List<WebDriver> mDriverList;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        mSelendroidServer = new SelendroidLauncher(new SelendroidConfiguration());
        mSelendroidServer.launchSelendroid();
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        mSelendroidServer.stopSelendroid();
    }

    @Before
    public void setUp() throws Exception {
        List<Task<WebDriver>> tasks = new ArrayList<>();

        JSONArray jsonArray = mSelendroidServer.getServer().getDriver().getSupportedDevices();
        for (int i = 0; i < jsonArray.length(); i++) {
            final String model = jsonArray.getJSONObject(i).getString("model");
            tasks.add(Task.callInBackground(new Callable<WebDriver>() {
                @Override
                public WebDriver call() throws Exception {
                    DesiredCapabilities selendroidCapabilities = SelendroidCapabilities.android();
                    selendroidCapabilities.setCapability("model", model);
                    SelendroidDriver driver = new SelendroidDriver(selendroidCapabilities);
                    driver.setConfiguration(DriverCommand.SEND_KEYS_TO_ELEMENT, "nativeEvents", false);
                    return driver;
                }
            }));
        }

        Task.whenAllResult(tasks).onSuccess(new Continuation<List<WebDriver>, Object>() {
            @Override
            public Object then(Task<List<WebDriver>> task) throws Exception {
                mDriverList = task.getResult();
                return null;
            }
        }).waitForCompletion();
    }

    @After
    public void tearDown() throws InterruptedException {
        List<Task<Object>> tasks = new ArrayList<>();
        for (final WebDriver driver : mDriverList) {
            tasks.add(Task.callInBackground(new Callable<Object>() {
                @Override
                public Object call() throws Exception {
                    driver.quit();
                    return null;
                }
            }));
        }
        Task.whenAll(tasks).waitForCompletion();
    }

    @Test
    @Repeat(times = 1000)
    public void shouldSearch() throws InterruptedException {
        List<Task<Object>> tasks = new ArrayList<>();
        for (WebDriver driver : mDriverList) {
            tasks.add(executeScript(driver));
        }
        Task.whenAll(tasks).waitForCompletion();
    }

    private Task<Object> executeScript(final WebDriver driver) {
        return Task.callInBackground(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                driver.get("http://m.naver.com");
                WebElement element = driver.findElement(By.id("query"));
                element.sendKeys("식탁의자");
                element.submit();
                Thread.sleep(1000);
                element = driver.findElement(By.xpath("//*[@id=\"_shopping_browse\"]/div[3]/div[6]/div/a/div[1]/img"));
                element.click();
                Thread.sleep(1000);
                return null;
            }
        });
    }
}